import React from "react";
import "../../style/scss/components/menu.scss"

// Importing third party components
import { connect } from "react-redux";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        console.log("This.props: ", this.props);
        return(
            <nav id="menu">
                <ul>
                    <li className="header">{ this.props.session.user && this.props.session.user.type === "admin" ? "Admin" : "Employee"}</li>
                    {
                        this.props.session.user && this.props.session.user.type === "admin" ?
                            <li><a href="/employee">Employee</a></li>
                            : 
                            null
                    }
                    <li><a href="/review">Review</a></li>
                </ul>
            </nav>
        )
    }
}

const mapPropsToState = (state) => ({
    session: state.session
})

const MenuComponent = connect(mapPropsToState)(Index);

export default MenuComponent;