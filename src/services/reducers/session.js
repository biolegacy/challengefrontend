const IntialState = {
    user: null
}

const SessionReducer = (state = IntialState, action) => {
    console.log("Payload", action);
    switch(action.type){
        case SessionAction.Set: {
            state = {
                ...state, 
                user: action.payload
            }
            return state;
        }
        case SessionAction.Clear: {
            state = {
                ...state,
                user: null
            }
            return state;
        }
        default: 
            return state;
    }
}

export const SessionAction = {
    Set: "Set",
    Clear: "Clear"
}

export default SessionReducer;