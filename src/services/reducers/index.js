import { combineReducers } from "redux";

// Import reducers
import SessionReducer from "./session";

export default combineReducers({
    session: SessionReducer
})