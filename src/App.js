import React from 'react';
import { BrowserRouter, Route} from "react-router-dom";

// Import pages
import Employee from "./pages/Employee";
import EmployeeView from "./pages/Employee/view";
import EmployeeEdit from "./pages/Employee/edit";

import Review from "./pages/Review";
import ReviewView from "./pages/Review/view";
import ReviewEdit from "./pages/Review/edit";

import Login from "./pages/Login"

import Intro from "./pages/Intro";

// Import store and Provider
import { connect, Provider } from "react-redux";
import store from "./services/store";

function App() {
    return (
      <BrowserRouter>
        <Route exact path={`/`} component={ Intro } />
        <Route exact path={`/login`} component={Login} />
        <Route exact path={`/employee`} component={Employee} />
        <Route exact path={`/employee/create`} component={EmployeeEdit} />
        <Route exact path={`/employee/view/:id`} component={EmployeeView}/>
        <Route exact path={`/employee/edit/:id`}  component={EmployeeEdit}/>
        <Route exact path={`/review`} component={Review} />
        <Route exact path={`/review/create`} component={ReviewEdit} />
        <Route exact path={`/review/view/:id`} component={ ReviewView } />
        <Route exact path={`/review/edit/:id`}  component={ReviewEdit}/>
      </BrowserRouter>
    );
}

const mapStateToProps = (state) => ({
  session: state.session  
})

const ConnectedApp = connect(mapStateToProps)(App);

export default () => {
  return(
    <Provider store={store}>
      <ConnectedApp />
    </Provider>
  )
};
