import React, { Component } from "react";
import "../../style/scss/Employee.scss"

// Importing custom components
import Menu from "../../components/menu"
import API from "../../API";
;
class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            employees: [
                {
                    id: 1,
                    name: "Employee 1",
                    department: "IT"
                },
                {
                    id: 2,
                    name: "Employee 2",
                    department: "Sales"
                },
                {
                    id: 3,
                    name: "Employee 3",
                    department: "IT"
                },
                {
                    id: 4,
                    name: "Employee 4",
                    department: "IT"
                },
                {
                    id: 5,
                    name: "Employee 5",
                    department: "Sales"
                },
            ]
        }
    }

    delete(){
        alert(`Succesfully deleted`);
    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <div className="content-header">
                        <h5 id="page-title">Employees</h5>
                        <div className="add-btn-container">
                            <a href={`/employee/create`} className="button add">CREATE</a>
                        </div>
                    </div>
                    <div id="employee-list-container">
                        <ul>
                            <li>
                                <div><span className="title">ID</span></div>
                                <div><span className="title">Employee name</span></div>
                                <div><span className="title">Department</span></div>
                                <div><span className="title">Options</span></div>
                            </li>
                            {
                                this.state.employees.map((employee, index) => {
                                    return(
                                        <li key={index}>
                                            <div>{employee.id}</div>
                                            <div style={{ justifyContent: "flex-start" }}><a href={`/employee/view/${1}`}>{employee.name}</a></div>
                                            <div style={{ justifyContent: "flex-start" }}><p>{employee.department}</p></div>
                                            <div className="options">
                                                <div className="option">
                                                    <a href={`/employee/edit/${employee.id}`} className="button">Edit</a>
                                                </div>
                                                <div className="option">
                                                    <div onClick={()=> { this.delete("Username") }} className="button delete">Delete</div>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                })
                            }
                            
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;