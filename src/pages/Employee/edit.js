import React, { Component } from "react";
import "../../style/scss/Employee.scss"

// Importing custom components
import Menu from "../../components/menu"

const PageSetting = {
    edit: {
        lable: "edit",
        title: "Employee profile - Edit"
    },
    create: {
        label: "create",
        title: "Create new employee"
    }
}

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            pageSetting: {},
            profile: {}
        }
    }

    componentDidMount(){
        this.ProcessUrl();
    }

    ProcessUrl(){
        const Url = window.location.href;
        const UrlParts = Url.split("/");
        if(UrlParts[UrlParts.length - 1] === "create"){
            this.setState({
                pageSetting: PageSetting.create
            })
        } else {
            // Fetch the employee information via API if necessary
            
            this.setState({
                pageSetting: PageSetting.edit
            })
        }
    }

    CreateOrUpdate(){
        if(this.state.pageSetting.label === "create"){
            // Create new employee
            console.log("Create new employee");
        } else {
            // Update employee profile
            console.log("Update employee profile");
        }
    }

    Cancel(){
        // Cancel the process, ask if the user really want to cancel the process before proceed
        console.log("Cancel!");
        // If user want to cancel it direct the user to employee list
        this.props.history.push("/employee");
    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <h5 id="page-title">{this.state.pageSetting.title}</h5>
                    <form className="form">
                        <div className="input-container">
                            <label>Employee Name</label>
                            <input type="text" placeholder={'Employee name...'} />
                        </div>
                        <div className="input-container">
                            <label>Department</label>
                            <input type="text" placeholder={'Department...'} />
                        </div>
                        <div className="controls-container">
                            <div className="button" onClick={() => { this.CreateOrUpdate()}}>{ this.state.pageSetting.label === "add" ? "Create" : "Update"}</div>
                            <div className="button" onClick={() => { this.Cancel() }}>Cancel</div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Index;