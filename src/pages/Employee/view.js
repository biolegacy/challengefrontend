import React, { Component } from "react";
import "../../style/scss/Employee.scss"

// Importing custom components
import Menu from "../../components/menu"

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <h5 id="page-title">Employee profile</h5>
                </div>
            </div>
        );
    }
}

export default Index;