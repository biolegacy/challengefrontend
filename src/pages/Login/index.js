import React from "react";
import { connect } from "react-redux";
import "../../style/scss/login.scss"

// Importing session actions
import { SessionAction } from "../../services/reducers/session";

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    Login(value){
        // depending on the value login as employee or admin
        if(value === "admin"){
            this.props.dispatch(dispatch => {
                dispatch({
                    type: SessionAction.Set,
                    payload: {
                        type: "admin"
                    }
                });
                this.props.history.push(`/review`);
        })
        } else {
            this.props.dispatch(dispatch => {
                dispatch({
                    type: SessionAction.Set,
                    payload: {
                        type: "employee"
                    }
                });
                this.props.history.push(`/review`);
            })
        }
    }

    render(){
        return(
            <div id="login-container">
                <div id="login-form">
                    <h5>Login</h5>
                    <div className="button" onClick={() => { this.Login("admin") }}>As Admin</div>
                    <div className="button" onClick={() => { this.Login("employee") }}>As Employee</div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    session: state.session
})

export default connect(mapStateToProps)(Index);