import React, { Component } from "react";
import "../../style/scss/Review.scss";

// Importing custom components
import Menu from "../../components/menu"
import { connect } from "react-redux";

class Index extends Component {
    constructor(props){
        super(props);
        this.state = {
            
        }
    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <div className="content-header">
                        <h5 id="page-title">
                            {
                                this.props.session.user && this.props.session.user.type === "admin" ? "Reviews" : "Pending reviews"
                            }
                        </h5>
                        <div className="add-btn-container">
                            <a href={`/review/create`} className="button add">CREATE</a>
                        </div>
                    </div>
                    <div id="review-list-container">
                        <ul>
                            <li>
                                <div className="title">ID</div>
                                <div className="title">Description</div>
                                <div className="title">Of</div>
                                <div className="title">By</div>
                                <div className="title">Assignee</div>
                            </li>
                            <li>
                                <div><a href={`/review/view/${1}`}>1</a></div>
                                <div><a href={`/review/view/${1}`}>Monthly performance review</a></div>
                                <div><a href={`/employee/view/${1}`}>Employee one</a></div>
                                <div><a href={`/employee/view/${1}`}>Admin one</a></div>
                                <div className="list-container">
                                    <a href={`/employee/view/${2}`}>Employee two</a>, 
                                    <a href={`/employee/view/${3}`}>Employee three</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

const MapStateToProps = (state) => ({
    session: state.session
})

export default connect(MapStateToProps)(Index);