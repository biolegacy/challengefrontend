import React from "react";
import "../../style/scss/Review.scss";

// Importing custom components
import Menu from "../../components/menu"

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data: {} // Review data
        }
    }

    GoToEdit(){
        this.props.history.push(`/review/edit/${1}`, {data: this.state.data})
    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <h5 id="page-title">Review</h5>
                    <div id="review">
                        <div className="info-section">
                            <label>Review of:</label>
                            <a href={`/employee/view/${1}`} className="bold">Employee one</a>
                        </div>
                        <div className="info-section">
                            <label>Issued by:</label>
                            <a href={`/employee/view/${1}`} className="bold">Admin one</a>
                        </div>
                        <div className="info-section">
                            <label>Description:</label>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in venenatis magna, et condimentum felis. Mauris eget risus accumsan, pharetra purus sed, porta mauris. Sed sit amet laoreet enim.</p>
                        </div>
                        <div className="info-section">
                            <label>Assignee:</label>
                            <div className="assignee-list">
                                <a href={`/employee/view/${2}`}>Assignee two</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>,
                                <a href={`/employee/view/${3}`}>Assignee three</a>
                            </div>
                        </div>
                        <div className="info-section">
                            <label>Last updated by:</label>
                            <a href={`/employee/view/${1}`} className="bold">Admin three</a>
                        </div>
                        <div className="info-section">
                            <label>Updated at:</label>
                            <h6>2020-11-03</h6>    
                        </div>
                    </div>
                    <div className="controls-container">
                        <div className="button" onClick={() => { this.GoToEdit() }}>Edit</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;