import React from "react";
import "../../style/scss/Review.scss";

// Importing custom components
import Menu from "../../components/menu"
import { connect } from "react-redux";

const PageSetting = {
    create: {
        title: "Create review",
        label: "create"
    },
    edit: {
        title: "Edit review",
        label: "edit"
    }
}

class Index extends React.Component{
    constructor(props){
        super(props);
        this.state={
            pageSetting: {},
            assignees: ['AssigneeOne', 'AssigneeTwo'],
            assigneeInput: ''
        }
    }

    componentDidMount() {
        this.ProcessUrl();
    }

    ProcessUrl(){
        const Url = window.location.href;
        const UrlParts = Url.split("/");
        console.log("url parts: ", UrlParts)
        if(UrlParts[UrlParts.length - 1] === "create"){
            this.setState({
                pageSetting: PageSetting.create
            })
        } else {
            // Fetch the employee information via API if necessary
            this.setState({
                pageSetting: PageSetting.edit
            })
        }
    }

    AddAssignee(){
        const updatedAssigneeList = this.state.assignees;
        updatedAssigneeList.push(this.state.assigneeInput);
        this.setState({
            assignees: updatedAssigneeList,
            assigneeInput: ''
        })
    }

    RemoveAssignee(index){
        const assigneeList = this.state.assignees;
        assigneeList.splice(index, 1);
        this.setState({
            assignee: assigneeList
        })
        
    }

    updateAssingeeInput(value){
        this.setState({
            assigneeInput: value
        })
    }

    CreateOrUpdate(){
        if(this.state.pageSetting.label === "create"){
            // Create new Review
            alert("Review has been created!");
            this.props.history.push(`/review`)
        } else {
            // Update the review
            alert("Review has been updated!");
            this.props.history.push(`/review`)
        }
    }

    Cancel(){

    }

    render(){
        return(
            <div id="canvas">
                <Menu />
                <div id="content">
                    <h5 id="page-title">{ this.state.pageSetting.title }</h5>
                    <form className="form">
                        <div className="input-container">
                            <label>Description</label>
                            <textarea placeholder={'Employee name...'}></textarea>
                        </div>
                        <div className="input-container">
                            <label>Employee</label>
                            <input disabled={this.state.pageSetting.label === "create" ? false : true} type="text" placeholder={'Enter employee...'} />
                        </div>
                        <div className="input-container">
                            <label>Assignee</label>
                            <input type="text" value={this.state.assigneeInput} placeholder={`Enter assignee's name`} onChange={(e) => { this.updateAssingeeInput(e.target.value) }} />
                            <div className="button" onClick={() => { this.AddAssignee() }}>Add</div>
                        </div>
                        <div className="input-container">
                            <label>Current assignees</label>
                            <div className="assignee-list" >
                                {
                                    this.state.assignees.map((assignee, index) => {
                                        return(
                                            <div key={index} className="assignee">
                                                <div onClick={() => { this.RemoveAssignee(index) }}>
                                                    {assignee}
                                                    <span>remove</span>
                                                </div>
                                                {
                                                    index + 1 === this.state.assignees.length ? '' : ', '
                                                }
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="controls-container">
                            <div className="button" onClick={() => { this.CreateOrUpdate()}}>{ this.state.pageSetting.label === "create" ? "Create" : "Update"}</div>
                            <div className="button" onClick={() => { this.Cancel() }}>Cancel</div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    session: state.session
})

export default connect(mapStateToProps)(Index);