import React from "react";

class Index extends React.Component{
    render(){
        return(
            <div>
                <ul>
                    <h4>Available paths</h4>
                    <li><a href="/login">Login</a></li>
                    <li><a href="/employee">Employee list ( Only for admin )</a></li>
                    <li><a href="/employee/create">Add Employee ( Only for admin )</a></li>
                    <li><a href="/employee/view/:id">View Employee profile</a></li>
                    <li><a href="/employee/edit/:id">Edit Employee ( Admin and Employee both can use)</a></li>
                    <li><a href="/review">List of Pending reviews</a></li>
                    <li><a href="/review/create">Create a review ( Admin and employee Both can use )</a></li>
                    <li><a href="/review/view/:id">View Review ( Admin and employee Both can use )</a></li>
                    <li><a href="/review/edit/:id">Edit review ( Admin and employee Both can use )</a></li>
                </ul>
            </div>
        )
    }
}

export default Index;