# How to run

## Follow the folowing steps
1. run `npm install` to install the necessary dependencies
2. run `npm start` to start the project

## How to use API component

API component located at `src/API`

if necessary set the header using the following function

    `API.setHeader({});`

`GET` request

    API.GET(`/valid`).then(resp => {
        // Process the data and manipulate for your needing
    })

`POST` request

    API.POST(`/register`, { username: 'username', password: 'password' }).then(resp => {
        // Handle the response
    }).catch(e => {
        // Write to log or ...
    })

`Delete and Patch follows the same flow as POST`
